#!/bin/bash

source /opt/maestro/maestro-test-scripts/test.conf

function stopTest() {
	cd /opt/maestro/maestro-test-scripts/scripts/groovy/utils/commands
	groovy Stop.groovy
	exit
}

parallel_counts=(1 10 100)
limit_destinations=(1 10 100)
message_sizes=(100)

cd /opt/maestro/maestro-test-scripts/scripts/groovy/singlepoint

echo "Checking if receiver data server is up"
curl --silent -o /dev/null http://maestro-receiver:8000
if [[ $? -ne 0 ]] ; then
	echo "The data server for the receiver is not running. Exit the container and re-run the command"
	exit 1
fi

echo "Checking if sender server is up"
curl --silent -o /dev/null http://maestro-sender:8000
if [[ $? -ne 0 ]] ; then
	echo "The data server for the sender is not running. Exit the container and re-run the command"
	exit 1
fi

trap stopTest SIGINT SIGTERM EXIT
echo "The first execution takes a while to kick-off due to downloading dependencies"

for parallel_count in ${parallel_counts[@]} ; do
	for limit_destination in ${limit_destinations[@]} ; do
		for message_size in ${message_sizes[@]} ; do
			if [[ $limit_destination -le $parallel_count ]] ; then
				echo "Testing parameters: count = ${parallel_count} / limit_destination = ${limit_destination} / message size = ${message_size}"

				export MESSAGE_SIZE=$message_size
				export PARALLEL_COUNT=${parallel_count}
				export LIMIT_DESTINATIONS=${limit_destination}

				export BROKER_URL="amqp://${BROKER_HOST}:5672/test.performance.queue?protocol=AMQP&durable=${DURABLE}&limitDestinations=${LIMIT_DESTINATIONS}"

				echo "Using the following URL for test ${BROKER_URL}"


				report_dir="/maestro/reports/c-${parallel_count}-ld-${limit_destination}-s-${message_size}"
				echo "The reports will be downloaded to ${report_dir}"
				groovy FixedRateTest.groovy ${report_dir}

				echo "Generating reports from ${report_dir}"
				/opt/maestro/maestro-cli/bin/maestro-cli report -d ${report_dir} -l info
			fi
		done
	done

done